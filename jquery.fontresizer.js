/* 
*	A plugin that resizes text responsively.
*	Author: Yoran Brondsema
*	GIT: https://bitbucket.org/ybrondsema/jquery-fontresizer
*/

(function($) {
	"use strict";
	
	var elements = [];
	var init = false;
	
	$.fn.fontresizer = function(options) {
		if (!init) {
			$(window).bind('load resize', function() {
				for (var i=0 ; i<elements.length ; i++) {
					var el = elements[i];
					
					var size = el.$el.width() * el.ratio;
					if (el.maxsize) {
						size = Math.min(size, el.maxsize);
					}
					if (el.minsize) {
						size = Math.max(size, el.minsize);
					}
					
					el.$el.css('font-size', size);
				}
			});
			
			init = true;
		}
	
		var settings = $.extend({
			maxsize: null,
			minsize: null
		},options);
		
		return this.each(function() {
			var $this = $(this);
			
			elements.push({
				$el: $this,
				ratio: settings.fontsize / settings.width,
				maxsize: settings.maxsize,
				minsize: settings.minsize
			});
		});
	};
})(jQuery);